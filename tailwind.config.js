// See https://tailwindcss.com/docs/configuration for details

module.exports = {
  purge: {
    content: ['./src/**/*.{js,jsx,ts,tsx}', './public/**/*.html'],
    // layers: ['base'],
  },
  // https://github.com/tailwindlabs/tailwindcss-forms
  plugins: [require('./node_modules/@tailwindcss/forms')],
  theme: {
    boxShadow: {
      sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
      DEFAULT: '16px 64px 64px -8px rgba(0, 0, 0, 0.08)',
      md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
      lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
      xl: '2px 24px 64px 4px rgba(0, 0, 0, 0.08)',
      '2xl': '24px 72px 128px 16px rgba(0, 0, 0, 0.08)',
      none: 'none',
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      primary: {
        0: '#F9FCF8',
        1: '#E6F4E2',
        2: '#ECF6E9',
        3: '#D1EDC9',
        4: '#76BB63',
        5: '#55B465',
        DEFAULT: '#4F9C3A',
        900: '#25441C',
      },
      secondary: {
        0: '#FF9563',
        1: '#FCECE7',
        4: '#D4623A',
        DEFAULT: '#E66437',
        9: '#AE3409',
      },
      footer: {
        light: '#E66437',
        DEFAULT: '#383E42',
        dark: '#26292C',
      },
      neutral: {
        0: '#FFFFFF',
        DEFAULT: '#FFFFFF',
        1: '#FAFAFA',
        2: '#EAEBEB',
        3: '#F5F5F5',
        4: '#A3A3A3',
        5: '#C0C4BF',
        7: '#505050',
        8: '#383E42',
        9: '#26292C',
      },
      accent: {
        1: '#388AC5',
        2: '#1F78B7',
        3: '#50544F',
        DEFAULT: '#293842',
      },
      brown: {
        DEFAULT: '#C9AC75',
        2: '#44261C',
      },
      white: {
        1: '#E5E5EA',
        3: '#F2F2F7',
        9: '#FFF',
        DEFAULT: '#FFF',
      },
      ash: {
        1: '#8E8E93',
        3: '#393E41',
        4: 'BBB4B5',
        DEFAULT: '#8E8E93',
      },
      black: '#000000',
      danger: '#F8D7DA',
      yellow: {
        1: '#FFEECC',
        5: '#E8AE56',
      },
    },
    extend: {
      padding: {
        '6px': '6px',
        '10px': '10px',
      },
      fontFamily: {
        regular: ['Poppins-Regular', 'sans-serif'],
        semiBold: ['Poppins-SemiBold', 'sans-serif'],
        extraBold: ['Poppins-ExtraBold', 'Poppins-SemiBold', 'sans-serif'],
        bold: ['Poppins-Bold', 'Poppins-ExtraBold', 'sans-serif'],
        'bangla-semiBold': ['HindSiliguri-SemiBold'],
        arabic: ['Hafs', 'sans-serif'],
        kalpurush: ['kalpurush', 'sans-serif'],
        'anek-bangla': ['anek-bangla', 'sans-serif'],
      },
      fontSize: {
        '7xl': [
          '4rem', // 64px
          {
            lineHeight: '4.25rem', // 68px
            letterSpacing: '-0.25rem', // -4px
          },
        ],
        '3xl': [
          '2rem', // 32px
          {
            lineHeight: '2.75rem', // 44px
            letterSpacing: '-0.008', // -0.8%
          },
        ],
      },
      borderRadius: {
        '4xl': '32px',
      },
    },
  },
};
