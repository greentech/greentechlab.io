## Development

#### Clone the Project

```
$ git clone https://gitlab.com/greentech/greentech.gitlab.io.git
```

#### Pull from `dev` branch or create your own

```bash
$ git pull origin dev
$ git checkout dev
```

#### Run locally

```bash
$ npm run develop  ---- run the server using
$ npm run build  ------ build project
$ npm serve ----------- serve
```

#### Push to upstream

`$ git push origin dev`

#### Whats inside

    .
    ├── node_modules
    ├── src
      ├── components
      ├── css
      ├── data
      ├── fonts
      ├── images
      ├── less
      ├── oneskyapp_files
      ├── pages
      ├── templates
      ├── tests
      └── utils
    ├── static
      ├── images
      ├── js
      └── particlesjs-config.json
    ├── .gitignore
    ├── .prettierrc
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    └── README.md

## Deploying

Create a merge request to `master` branch
[Netlify](https://www.netlify.com) is configured to automatically build and serve from the `master` branch

### 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.org/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.
