const redirects = require('./redirects.json');

exports.onCreatePage = ({ page, actions }) => {
  const { deletePage, createRedirect } = actions;

  redirects.forEach((redirect) => {
    if (redirect.fromPath == page.path) {
      deletePage(page);
      createRedirect({
        fromPath: redirect.fromPath,
        toPath: redirect.toPath,
        isPermanent: true,
      });
    }
  });
};

const resolvers = {
  AppList: {
    appLogoImageFile: {
      type: 'File',
      resolve: async (source, args, context) => {
        const node = await context.nodeModel.runQuery({
          query: {
            filter: {
              relativePath: { eq: source.appLogo },
            },
          },
          type: 'File',
          firstOnly: true,
        });

        return node;
      },
    },
    previewImageFile: {
      type: 'File',
      resolve: async (source, args, context) => {
        const node = await context.nodeModel.runQuery({
          query: {
            filter: {
              relativePath: { eq: source.previewImage },
            },
          },
          type: 'File',
          firstOnly: true,
        });

        return node;
      },
    },
    heroImageFile: {
      type: 'File',
      resolve: async (source, args, context) => {
        const node = await context.nodeModel.runQuery({
          query: {
            filter: {
              relativePath: { eq: source.heroImage },
            },
          },
          type: 'File',
          firstOnly: true,
        });

        return node;
      },
    },
    listImageFile: {
      type: 'File',
      resolve: async (source, args, context) => {
        const node = await context.nodeModel.runQuery({
          query: {
            filter: {
              relativePath: { eq: source.listImage },
            },
          },
          type: 'File',
          firstOnly: true,
        });

        return node;
      },
    },
    smallImageFile: {
      type: 'File',
      resolve: async (source, args, context) => {
        const node = await context.nodeModel.runQuery({
          query: {
            filter: {
              relativePath: { eq: source.smallImage },
            },
          },
          type: 'File',
          firstOnly: true,
        });

        return node;
      },
    },
    desktopDownloadPreviewImageFile: {
      type: 'File',
      resolve: async (source, args, context) => {
        const node = await context.nodeModel.runQuery({
          query: {
            filter: {
              relativePath: { eq: source.desktopDownloadPreviewImage },
            },
          },
          type: 'File',
          firstOnly: true,
        });

        return node;
      },
    },
  },
  AppListWhatYouGet: {
    imageFile: {
      type: 'File',
      resolve: async (source, args, context) => {
        const node = await context.nodeModel.runQuery({
          query: {
            filter: {
              relativePath: { eq: source.image },
            },
          },
          type: 'File',
          firstOnly: true,
        });

        return node;
      },
    },
  },
  collaborationPartners: {
    imageFile: {
      type: 'File',
      resolve: async (source, args, context) => {
        const node = await context.nodeModel.runQuery({
          query: {
            filter: {
              relativePath: { eq: source.image },
            },
          },
          type: 'File',
          firstOnly: true,
        });

        return node;
      },
    },
  },
};

exports.createResolvers = async ({ createResolvers }) => {
  createResolvers(resolvers);
};
