module.exports = {
  ci: {
    collect: {
      staticDistDir: './public',
      url: ['http://localhost:8080', 'http://localhost:8080/donate/'],
    },
    upload: {
      target: 'temporary-public-storage',
    },
  },
};
