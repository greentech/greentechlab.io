const tailwindConfig = require('./tailwind.config.js');

require('dotenv').config({
  path: `.env`,
});

const faqSearchQueries = `{
 items: allWpFaq {
    edges {
      node {
        databaseId
        content
        title
        slug
        faq {
          category
        }
        internal {
          contentDigest
        }
      }
    }
  }
}`;

const blogQueries = `{
  items: allWpPost {
    edges {
     node {
       databaseId
       content
       title
       slug
       internal {
        contentDigest
       }
     }
   }
 }
}`;

const appQueries = `{
  items: 
    allAppList {
      edges {
        node {
          appId
          appName
          description
          route
          internal {
            contentDigest
          }
        }
      }
    }
}`;

function qaToAlgoliaRecord({ node: { databaseId, title, content, slug } }) {
  return {
    objectID: `faq-${databaseId}`,
    title,
    body: content,
    _tags: 'FAQ',
    path: `/support/${slug}/`,
  };
}

function blogToAlgoliaRecord({ node: { databaseId, content, slug, title } }) {
  return {
    objectID: `blog-${databaseId}`,
    title,
    body: content,
    path: `/blog/${slug}/`,
    _tags: 'blog',
  };
}

function appToAlgoliaRecord({ node: { appId, appName, description, route } }) {
  return {
    objectID: `app-${appId}`,
    title: appName,
    body: appName + ' ' + description,
    path: `/apps/${route}/`,
    _tags: 'app',
  };
}

const queries = [
  {
    query: faqSearchQueries,
    transformer: ({ data }) => data.items.edges.map(qaToAlgoliaRecord),
    settings: {},
    matchFields: [],
  },
  {
    query: blogQueries,
    transformer: ({ data }) => data.items.edges.map(blogToAlgoliaRecord),
    settings: {},
    matchFields: [],
  },
  {
    query: appQueries,
    transformer: ({ data }) => data.items.edges.map(appToAlgoliaRecord),
    indexName: 'gtaf_website_pages', // overrides main index name, optional
    settings: {},
    matchFields: [],
  },
];

module.exports = {
  trailingSlash: 'always',
  siteMetadata: {
    title: 'Greentech Apps Foundation',
    description:
      'Source for authentic, modern Islamic Applications. We are dedicated to providing good quality and beneficial Islamic applications for the world.',
    keywords:
      'islam, islamic, quran, hadith, application, prayer, dua, zikr, android, religion, muslim, apps',
    author: 'Greentech Apps Foundation',
    siteUrl: `https://gtaf.org`,
    url: 'https://gtaf.org', // No trailing slash allowed!
    image: '/images/logo_og.png', // Path to the image placed in the 'static' folder, in the project's root directory.
    twitterUsername: '@greentechapps',
    titleTemplate: '%s | Greentech Apps Foundation',
  },
  plugins: [
    `gatsby-plugin-eslint`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Greentech Apps Foundation',
        short_name: 'greentechapps',
        start_url: '/',
        background_color: '#469910',
        theme_color: '#189939',
        display: 'minimal-ui',
        icon: 'static/images/logo_black.png', // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-postcss`,
      options: {
        postCssPlugins: [
          require(`tailwindcss`)(tailwindConfig),
          require(`autoprefixer`),
          ...(process.env.NODE_ENV === `production`
            ? [require(`cssnano`)]
            : []),
        ],
      },
    },
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        printRejected: true, // Print removed selectors and processed file names
        develop: false, // Enable while using `gatsby develop`
        tailwind: true, // Enable tailwindcss support
        purgeCSSOptions: {
          safelist: {},
          // More options defined here https://purgecss.com/configuration.html#options
        },
        ignore: ['/styles/global.css'],
      },
    },
    {
      resolve: `gatsby-transformer-json`,
      options: {
        // eslint-disable-next-line no-unused-vars
        typeName: ({ node, object, isArray }) => {
          return node.name;
        },
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data`,
      },
    },
    `gatsby-plugin-remove-serviceworker`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/static/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        url: `https://cms-internal.gtaf.org/graphql`,
        schema: {
          timeout: 120000, // 120 seconds
        },
        html: {
          useGatsbyImage: false,
        },
        type: {
          MediaItem: {
            excludeFieldNames: [`seo`],
          },
        },
      },
    },
    {
      // This plugin must be placed last in your list of plugins
      // to ensure that it can query all the GraphQL data
      resolve: `gatsby-plugin-algolia`,
      options: {
        appId: process.env.ALGOLIA_APP_ID,
        apiKey: process.env.ALGOLIA_API_KEY,
        indexName: process.env.ALGOLIA_INDEX_NAME,
        queries,
        chunkSize: 10000, // default: 1000
        settings: {},
        enablePartialUpdates: false,
        matchFields: ['slug', 'modified'],
        concurrentQueries: false, // default: true
        skipIndexing: false,
      },
    },
    `gatsby-plugin-advanced-sitemap`,
    {
      resolve: 'gatsby-plugin-webpack-bundle-analyser-v2',
      options: {
        devMode: true,
      },
    },
    {
      resolve: 'gatsby-plugin-anchor-links',
      options: {
        offset: -100,
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /\.inline\.svg$/,
        },
      },
    },
  ],
};
