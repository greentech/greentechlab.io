---
tags: []
path: newsletter-ramadan-2022
date: 
title: Newsletter | Ramadan 2022
subtitle: ''
topics: []
isFeatured: false
banner: ''

---
Bismillahir rahmanir rahim.

Assalamu ‘alaykum wa rahmatullah,

Hope you are doing well. Alhamdulillah, we have almost reached the end of Ramadan, and we wanted to share with you our achievements since last Ramadan.

**Al Quran (Tafsir & by Word)**

1. Bookmark redesign with search & sort
2. User profile to sync bookmark & notes
3. Word by word audio highlighting
4. Vietnamese and Kinyarwanda Translations
5. Ali al-Hudhaify, Ayman Suwaid & more reciters
6. Launched on the macOS platform

  
**Hadith**

1. UI improvements
2. Several content corrections & book additions

  
**Deen Quiz**

1. Added highly requested English quiz contents
2. Launched on the iOS platform

**  
Seerah**

1. Commenced designing a brand-new Seerah app

  
**Our goals for 2022,**

1. Sync mechanism for Hadith and Dua app
2. Revamp Dua app
3. Launch Seerah app
4. Implement progress tracking & stats for the Quran app
5. Improve support system
6. And many more!

We wanted to thank you for your endless support and valuable contributions! Our work would not have been possible without Allah’s help, your duas and continuous donations.

Please donate via gtaf.org/donate to be a part of this sadaqah jariyah project and earn rewards. Don’t forget to share our apps with your friends and family.

JazakAllahu khairan.

Regards,

[GTAF.org](https://gtaf.org/) Team