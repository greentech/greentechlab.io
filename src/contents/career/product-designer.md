---
path: product-designer
designation: Product Designer 
category: career
formLink: https://forms.clickup.com/3405139/f/37xak-15488/NALP8GFNFMYG8GS2A4
department: Design
nature: Remote
---

## Context

### Offered Position

Product Designer 

### About Us

**Greentech Apps Foundation** ([gtaf.org](https://gtaf.org)) is a UK-based charity building Islamic applications for the benefit of Mankind. We are a team striving our best to cater to the Islamic needs of the Ummah in this modern, digital world. We currently have 7+ apps on Android, iOS, Web, macOS & Windows and over a million users.

### Address

20 Lilac Court, Cambridge CB1 7AY, UK

## Details

### About the Role

As a Product Designer for Greentech Apps Foundation, you will be responsible for creating and developing new product designs, as well as improving and refining existing products. This includes creating design concepts, prototyping, and testing. The product designer will work closely with cross-functional teams such as engineers, marketers, and product managers to ensure that the product meets the needs of the target audience and aligns with the company's goals. Strong skills in design thinking, problem-solving, and communication are essential for success in this role.

### Responsibilities
- Participate in user research and interaction studies.
- Present rough drafts to product managers and or key stakeholders to gain approval.
- Translate user stories and business requirements into effective designs.
- Design sitemap, wireframes, prototypes and UI.
- Design UI mockups and prototypes that clearly illustrate how apps function and look.
- Identify and troubleshoot UX/UI problems.
- Collaborate with other designers to evolve our brand-identity style guide.
- Design marketing materials (E.g. Social media post̒ and newsletter) for growth.
- Stay up-to-date with design application
- Conducting user testing to gather feedback on designs
- Collaborating with developers to ensure that designs are implemented correctly
- Assisting with defining and documenting design processes and guidelines
- Participating in stakeholder meetings to present and defend design decisions
- Creating and maintaining design systems and style guides
- Managing and prioritizing design tasks and projects within a team or project schedule
- Continuously learning about new design trends, technologies, and tools
- Participating in design reviews and providing feedback to other designers on their work


### Employment Status

Full-Time

### Requirements

#### Preferred Background

- BSc/BBA or any similar degrees
- A strong portfolio that showcases candidate’s design skills and experience working on a range of projects
- 1+ years of meaningful experience in a similar role


#### Skill Requirements

- Strong understanding of design principles and techniques
- Proficiency in Figma design software (experience other softwares is a plus)
- Experience with user research, prototyping, and product testing
- Ability to work in a team and collaborate with cross-functional teams such as engineers, marketers, and project managers
- Strong communication and presentation skills to effectively convey design concepts and ideas
- Ability to think creatively, problem-solve, and generate innovative ideas
- Familiarity with user-centered design methodologies and design thinking process


### Location

Remote (Based in Bangladesh)

### Salary

30,000 - 50,000 BDT

### Compensation & Other Benefits

- Yearly salary review
- 2 weekly holidays (Friday & Saturday)
- Budget and culture for continued learning and up-skilling
- 2 yearly festival bonuses
- Casual leave, sick leave, parental leave and public holidays as gazetted by the local government and organization’s decision


### Here you will

- Work with a team of ethically motivated people to create beneficial applications.
- Learn things that will help you grow both as a product manager and an individual.
- Help to create products that will benefit humanity.
