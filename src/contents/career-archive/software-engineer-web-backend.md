---
path: software-engineer-web-mid-backend
designation: Software Engineer (Web Backend)
category: career
---

## Context

### Offered Position

**Software Engineer**

### About Us

**Greentech Apps Foundation** ([gtaf.org](https://gtaf.org)) is a UK based charity, building Islamic applications for the benefit of Mankind. We are a team striving our best to cater to the Islamic needs of the Ummah in this modern, digital world. We currently have 7+ apps on Android, iOS, Web, macOS & Windows and over a million users.

### Address

20 Lilac Court, Cambridge CB1 7AY, UK

## Details

### Responsibilities

- Maintain and add functionality to the existing web apps and projects.
- Requirement analysis and providing estimations.
- Coordinating closely with team leads, project managers, testers, and other developers to design, code, test, deliver, and maintain software modules in an agile scrum environment.
- Design & build backend database and APIs for web.
- Deploy and publish applications in servers.
- Provide schedule maintenance of backend servers.
- Work on bug fixing and improving legacy application performance.
- Continuously explore the latest features, implement more modern technologies with correcting the bottlenecks.

### Employment Status

Full-time

### Requirements

#### Educational

- BSc/MSc or equivalent degree (preferably in Computer Science)

#### Expertise

- 1+ years of experience working with web backend
- Proficiency of any programming language (e.g. Python, C, C++, Java, Golang etc.)
- Working knowledge of HTML, CSS, JavaScript, Django framework.
- Clear conception about fundamentals of web.
- Clear conception about the database concepts.
- Ability to design a system (database, API etc.) from a given requirement.
- Experience in any Unix operating system (Ubuntu, Debian, etc.).
- Experience in working with Agile environments.
- Good grip of quality code and best practices.
- Ability to identify and solve problems independently.
- Experience in working with version control systems (e.g. Git).
- Proficient in English for both written and verbal communication.

#### Good to have

- Working knowledge of NodeJs/Express.
- Hands on experience in DevOps pipeline.
- Experience in working with production-level applications.

### Location

Remote

### Salary

40k-60k BDT (Depending on expertise)

### Compensation & Other Benefits

- Yearly salary review.
- 2 yearly festival bonuses.
- 2 weekly holidays (Friday, Saturday).
- Casual leave, sick leave, annual leave, and public holidays as gazetted by the government and organization’s decision.

### Here you will

- Work with a team of ethically motivated people to create beneficial applications.
- Learn things that will help you grow both as an engineer and an individual.
- Help to create products that will benefit humanity.
