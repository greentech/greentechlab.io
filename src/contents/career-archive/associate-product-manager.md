---
path: associate-product-manager
designation: Associate Product Manager
category: career
formLink: https://forms.clickup.com/3405139/f/37xak-8383/ZG9NLIZXDUZXU3UQT0

---
## Context

### Offered Position

**Associate Product Manager**

### About Us

**Greentech Apps Foundation** ([gtaf.org](https://gtaf.org)) is a UK based charity, building Islamic applications for the benefit of Mankind. We are a team striving our best to cater to the Islamic needs of the Ummah in this modern, digital world. We currently have 7+ apps on Android, iOS, Web, macOS & Windows and over a million users.

### Address

20 Lilac Court, Cambridge CB1 7AY, UK

## Details

### About the Role

As an Associate Product Manager, you will be responsible for collecting product data, doing user research, coordinating with stakeholders, and developing new product features. You will work closely with our Product Manager and support them to work more effectively.

### Responsibilities

* Define product requirements and align them with organisation goals
* Understand user pain points and identify the problems they are facing
* Break down user problems into parts and create iterative solutions for the problems
* Collect and interpret both quantitative and qualitative product data and metrics
* Identify relevant opportunities by synthesizing user research, market research, and industry trends
* Facilitate the creation of compelling solutions by leveraging product intuition, user-generated insights, and the cross-functional team's expertise
* Coordinate cross-functionally with the product team and relevant stakeholders to deliver outstanding outcomes and achieve product vision
* Develop and implement a strategy that can contribute to the success of the projects
* Create a system to evaluate the success of the project and share the results or findings

### Employment Status

Full-time

### Requirements

#### Educational

* BSc/MSc or equivalent degree (preferably in Computer Science)
* Final year students are encouraged to apply

#### Expertise

* Proficient in English for both written and verbal communication
* Self-driven, Motivated and Goal-oriented
* Ability to identify and solve problems independently
* Ability to communicate to users with empathy
* Good analytical skills to make data-informed decisions
* Proficiency with Google G-Suite Products (Eg. Google Docs, Google Sheets)

#### Good to have

* Good sense of attention to detail
* Good UI/UX sense
* Prior experience in working on a similar role
* Experience in working in a team

### Location

Remote

### Salary

25-40k BDT (Depending on expertise)

### Compensation & Other Benefits

* Yearly salary review
* 2 yearly festival bonuses
* 2 weekly holidays (Friday & Saturday)
* Casual leave, sick leave, parental leave and public holidays as gazetted by the local government and organization’s decision
* Budget and culture for continued learning and up-skilling

### Here you will

* Work with a team of ethically motivated people to create beneficial applications
* Learn things that will help you grow both as a product manager and an individual
* Help to create products that will benefit humanity