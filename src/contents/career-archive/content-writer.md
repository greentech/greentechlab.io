---
path: content-writer
designation: Content Writer
category: career
department: marketing
nature: remote
formLink: https://forms.clickup.com/3405139/f/37xak-13648/VNHVZ5ANDU3S83O3U8
---

## Context

### Offered Position

**Content Writer**

### About Us

**Greentech Apps Foundation** ([gtaf.org](https://gtaf.org)) is a UK based charity, building Islamic applications for the benefit of Mankind. We are a team striving our best to cater to the Islamic needs of the Ummah in this modern, digital world. We currently have 7+ apps on Android, iOS, Web, macOS & Windows and over a million users.

### Address

20 Lilac Court, Cambridge CB1 7AY, UK

## Details

### About the Role

We are looking for a content writer to join our team. You will be responsible for developing content in the form of blog posts, articles, landing page content and additional marketing & business content (email copy, etc.).
You will be an integral part of our overall content marketing efforts to support our community and clients.

To be successful in this role, you will need to be capable of performing basic research when needed and have the ability to craft engaging content that is industry-specific and tailored to reach specific audiences. You also need to be able to edit/review content as required.

### Responsibilities

- Write copy for a variety of channels including social, blogs, emails, and web
- Edit and proofread work to ensure high standards are met across all content outputs
- Collaborate with other teams in the organisation such as marketing and product on a variety of projects
- Drive brand consistency across all company communications
- Develop and implement brand tone guidelines
- Stay current on trends and competitors within the industry
- See projects through the whole creative lifestyle, from inception to deployment
- Write clear, attractive copy with a distinct voice
- Conduct high-quality research
- Use SEO principles to maximize reach

### Employment Status

Full-time/Part-time

### Requirements

## Preferred Background

- BS or MS in marketing, business, or an IT field
- 1+ years of meaningful experience in a similar role
- Familiarity with Google suite (Docs, Sheets etc.)
- Aware of general Muslim community nuances/challenges

## Skill Requirements

- Adaptable in writing for a broad range of channels, from punchy Instagram captions to long-form blog posts
- Strong creative thinking skills and ability to think conceptually
- Comfortable working independently with little direction under tight deadlines
- Good time management skills
- Strong communication skills
- Being able to work in a team
- Excellent writing, editing, and proofreading skills with a diligent eye for detail, language, flow, and grammar
- Proven ability to demonstrate brand voice
- Strong attention to detail
- Good portfolio of work

### Location

Remote (Based in Bangladesh)

### Salary

25,000 - 28,000 BDT (Hourly if part time)

### Compensation & Other Benefits

- Yearly salary review
- 2 weekly holidays (Friday & Saturday)
- Budget and culture for continued learning and up-skilling
- 2 yearly festival bonuses\*
- Casual leave, sick leave, parental leave and public holidays as gazetted by the local government and organization’s decision\*

\* For full time contract only

### Here you will

- Work with a team of ethically motivated people to create beneficial applications
- Learn things that will help you grow both as a product manager and an individual
- Help to create products that will benefit humanity
