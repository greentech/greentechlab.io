---
designation: Software Engineer Intern (Mobile)
category: career
---

## Context

### Offered Position

**Software Engineer (Intern)**

### About Us

**Greentech Apps Foundation** ([gtaf.org](https://gtaf.org)) is a UK based charity, building Islamic applications for the benefit of Mankind. We are a team striving our best to cater to the Islamic needs of the Ummah in this modern, digital world. We currently have 7+ apps on Android, iOS, Web, macOS & Windows and over a million users.

### Address

20 Lilac Court, Cambridge CB1 7AY, UK

## Details

### Responsibilities

- Learning mobile application development basics and best practices and implementing in real life applications.
- Bug fixing and improving functionality of the existing mobile apps and projects.
- Coordinating closely with managers in an agile scrum environment.
- Design & build advanced application for a variety of platforms
- Work on bug fixing and improving legacy application performance.
- Continuously explore the latest features, implement more modern technologies with correcting the bottlenecks.

### Employment Status

Full-time

### Requirements

#### Educational

- BSc/MSc or equivalent degree (preferably in Computer Science)
- Final year students can also apply

#### Expertise

- Good grip on any programming language (C, C++, Java, Python etc.)
- Good problem solving skills
- Ability to identify and solve problems independently
- Proficient in English for both written and verbal communication

#### Good to have

- Working knowledge of Android(Java, Kotlin) / Flutter(Dart) / iOS(Swift)
- Good sense of attention to detail
- Experience in working with version control systems (e.g. Git)

### Location

Remote

### Salary

25k BDT (Depending on expertise)

### Compensation & Other Benefits

- Yearly salary review
- 2 yearly festival bonuses.
- 2 weekly holidays (Friday, Saturday).
- Casual leave, sick leave, annual leave, and public holidays as gazetted by the government and organization’s decision.

### Here you will

- Work with a team of ethically motivated people to create beneficial applications.
- Learn things that will help you grow both as an engineer and an individual.
- Help to create products that will benefit humanity.
