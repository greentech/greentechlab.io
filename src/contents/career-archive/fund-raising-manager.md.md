---
path: fund-raising-manager
designation: Fund Raising Manager
category: career
formLink: https://forms.clickup.com/3405139/f/37xak-13648/VNHVZ5ANDU3S83O3U8
department: marketing
nature: hybrid
---

## Context

### Offered Position

**Fundraising Manager (UK based, remote)**

### About Us

**Greentech Apps Foundation** ([gtaf.org](https://gtaf.org)) is a UK-based charity building Islamic applications for the benefit of Mankind. We are a team striving our best to cater to the Islamic needs of the Ummah in this modern, digital world. We currently have 7+ apps on Android, iOS, Web, macOS & Windows and over a million users.

### Address

20 Lilac Court, Cambridge CB1 7AY, UK

## Details

### About the Role

As the Fundraising Manager for Greentech Apps Foundation, you will be responsible for developing and implementing fundraising strategies to support the growth and sustainability of our organisation. This includes building relationships with donors and sponsors, researching grant opportunities, creating fundraising campaigns and representing the organisation in fundraising events.

To be successful in this role, you should have a strong background in fundraising, as well as excellent communication and relationship-building skills. Experience in the UK charity sector, particularly in Islamic education, is highly desirable.

### Responsibilities

- Developing and implementing a fundraising plan to support the organisation's mission and goals
- Researching and identifying potential donors, sponsors, and grant opportunities
- Building relationships with current and potential donors through networking and outreach efforts
- Creating and executing fundraising campaigns, including crowdfunding and events
- Managing and tracking fundraising efforts, including maintaining accurate records and reports
- Collaborating with the marketing team to create materials and campaigns that effectively communicate the organisation's mission and impact
- Working closely with the leadership team to ensure that fundraising efforts align with the organization's overall strategy

### Employment Status

Part-Time/Hourly

### Requirements

#### Preferred Background

- Undergraduate degree in Marketing, Business, or an IT field
- 1+ years of meaningful experience in a similar role
- Have connections in the UK Muslim community
- Is active in the UK dawah scene

#### Skill Requirements

- Comfortable working independently with little supervision
- Strong communication skills
- Proven ability to demonstrate brand voice

### Location

Remote, anywhere in the UK, preferably near London

### Salary

15£ (Hourly)

### Compensation & Other Benefits

- Yearly salary review
- Budget and culture for continued learning and up-skilling

### Here you will

- Work with a team of ethically motivated people to create beneficial applications.
- Learn things that will help you grow both as a product manager and an individual.
- Help to create products that will benefit humanity.
