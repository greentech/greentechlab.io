---
designation: Software Engineer (Web Frontend)
category: career
---

## Context

### Offered Position

**Software Engineer**

### About Us

**Greentech Apps Limited** is a UK based company, we are the people behind Greentech Apps Foundation [gtaf.org](https://gtaf.org). We provide software solutions to businesses and non profit organisations with best quality designs and products.

### Address

20 Lilac Court, Cambridge CB1 7AY, UK

## Details

### Responsibilities

- Maintain and add functionality to the existing web apps and projects.
- Requirement analysis and providing estimations.
- Coordinating closely with team leads, project managers, testers, and other developers to design, code, test, deliver, and maintain software modules in an agile scrum environment.
- Design & build advanced application for a variety of platforms
- Deploy and publish applications in servers.
- Work on bug fixing and improving legacy application performance.
- Continuously explore the latest features, implement more modern technologies with correcting the bottlenecks.

### Employment Status

Full-time

### Requirements

#### Educational

- BSc/MSc or equivalent degree (preferably in Computer Science)

#### Expertise

- 2+ years of experience working with web applications
- Working knowledge of HTML, CSS, JavaScript, ReactJs.
- Experience in working with Agile environments
- Good grip of quality code and best practices
- Ability to identify and solve problems independently
- Good sense of attention to detail
- Experience in working with version control systems (e.g. Git)
- Proficient in English for both written and verbal communication

#### Good to have

- Working knowledge of NextJs, TypeScript, and TailwindCSS
- Good UI/UX sense
- A taste for design and drive to achieve pixel perfect UI components.
- Experience in working with production-level applications

### Location

Remote

### Salary

45k-60k BDT (Depending on expertise)

### Compensation & Other Benefits

- Yearly salary review
- 2 yearly festival bonuses.
- 2 weekly holidays (Friday, Saturday).
- Casual leave, sick leave, annual leave, and public holidays as gazetted by the government and organization’s decision.

### Here you will

- Work with a team of ethically motivated people to create beneficial applications.
- Learn things that will help you grow both as an engineer and an individual.
- Help to create products that will benefit humanity.
