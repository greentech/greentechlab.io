---
tags: []
path: the-journey-greentech-apps-foundation
date: 2022-04-15T00:00:00.000+06:00
title: The Journey | Greentech Apps Foundation
subtitle: ''
topics: []
isFeatured: true
banner: "/media/rsz_1john-towner-3kv48ns4wuu-unsplash-1.jpg"

---
This is our story. We are Greentech Apps Foundation! And we are on a mission to encourage Islamic education and fulfill Islamic digital needs worldwide. We now have 7 apps, with no-ads, a million plus users with a big dream! Please watch the video below and get to know us better in shaa Allah.   
  
Want to be a part of this Sadaqah Jariyah project and earn continuous rewards from Allah SWT? Please come forward and support our cause!

To become a monthly donor, click [here](https://cutt.ly/uFGunCX).

<div style="padding-top: 56.25%; position: relative;overflow: hidden; width: 100%;"> <iframe style="position: absolute; top: 0; left: 0; bottom: 0; right: 0; width: 100%; height: 100%;" src="https://www.youtube.com/embed/fdfSKHAezDg" title="Greentech Apps Foundation | Donate Now" frameBorder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen loading="lazy"</iframe></div>