---
tags: []
path: our-achievements-in-q3-2022
date: 2022-10-30T00:00:00+06:00
title: "Our Achievements in Q3 2022! \U0001F680"
subtitle: ''
topics:
  - Report
isFeatured: true
banner: '/media/afif-ramdhasuma-rjqck9mqhng-unsplash-2.jpg'
---

Bismillahir rahmanir rahim.

Assalamu ‘alaykum!

May Allah reward you for your continuous support. We wanted to share a few of our notable achievements in the last quarter.

**New Hires:**

We have added two more Mobile Developers in our team and a Senior Backend Developer. Please welcome Saad, Abid and Sayem into the team. May Allah bless them to perform well for the Ummah.

**Brand New Launch:**

1. Seerah of Prophet Muhammad ﷺ

Learn about the Life of the Prophet Muhammad ﷺ, from the day he was born until the day he died, Arab’s prior history, lessons from events, quick summaries and more from this app.

Download app: [gtaf.org/apps/seerah](gtaf.org/apps/seerah 'gtaf.org/apps/seerah')

2. User Feedback Platform

We have set up a platform to get you more connected with us. Using this you can request new features, inform us about bugs on our apps. You will also get notified about the tasks we work on.

User Feedback Platform link: [feedback.gtaf.org](feedback.gtaf.org 'feedback.gtaf.org')

**Dua App:**

1. New Categories

2. New Duas

3. Several Bug Fixes

**Quran App:**

1. Search Mechanism Issue Fixed

2. Several Sync Issues Fixed

**Upcoming Releases:**

1. Hadith Sync and Login

2. Quran Stats

3. Quran Audio Word by Word

4. Quran Windows Desktop

5. Seerah Bookmarks and Dark Mode

6. Collaboration with Quran.com to bring in more Word by Word Audio Timestamps. We have created a Deep Learning Model to create Word by Word Timestamps for any reciter. This allows us to add any new reciters into the app in sha Allah.

**Content:**

1. We are working on fixing the issues in Tafsir Ibn Kathir (English).

2. We are also adding Word by Word in English on our Dua (Hisnul Muslim) app.

**RND:**

We are investigating a novel Tajweed Correction System using Artificial Intelligence.

We wanted to thank you for your endless support and valuable contributions!

Regards,

[GTAF.org](https://gtaf.org/ 'GTAF.org') Team
