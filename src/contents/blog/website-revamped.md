---
tags:
- islam
- new
- gtaf
- website
path: website-revamped
date: 2021-08-15T18:00:00.000+00:00
title: Website Revamped!
subtitle: ''
topics:
- non-profit
- organization
isFeatured: true
banner: "/media/website-revamped.jpg"

---
We are excited to announce the launch of our revamped website, alhamdulillah! Our team has worked hard for the past few months to give our website a fresh and modernized look. Our objective with the new website is to create a user-friendly experience for everyone. Along with being user-friendly, we wanted to make the new website faster, cleaner and easier to navigate.**

## Homepage

The new homepage allows you to go through our apps with more convenience. We made sure to add drop-down menus to each app, enabling you to choose from the list of available platforms, and download them on the platform of your choice. We receive numerous requests and queries on a daily basis regarding the availability of our apps on different platforms. We hope the clarity of our newly designed landing page makes it easier for you.

## **Discover**

The 'discover' section is the newest addition to our website. Under this section, as the title suggests, you will get to discover us! Starting from our achievements, goals and new updates to spreading awareness, knowledge and the message of Islam, you can now read our blogs and connect with us better in shaa Allah!

## **Organization**

If you want to know more about us, this is the place you would want to go to. The 'about us' section tells you about our mission, vision and goals. You will get an overview of who we are, what we do and what we have achieved so far!

The ‘contact form’, so far, has been the key mode of communication between us. You will always have the option to reach us through it in case you have an important query.

‘Career and volunteer’ tab is something that we have newly added. Alhamdulillah, as we grow, we also need to strengthen our workforce. You will find new job openings and volunteering opportunities under this section in shaa Allah.

## **Help and Support**

One of the most important additions would be the 'help and support' section. A website can never be complete without a help and support tab! You will now get introduced to all the FAQs. Our team has analysed and compiled a set of questions that have been frequently asked by you all. The FAQ section will also have a few short step-by-step tutorials on some of our key features. We hope to make the experience smoother and useful for you.

## **Donate**

Greentech Apps Foundation is sailing strong thanks to the blessings of Allah (SWT) and the endless support and generous donations from you. We have given the donation page new colours, bigger fonts and designs. To ensure user convenience, we have added some FAQs right next to the donation section. You can choose to contribute one-time or even monthly to aid our maintenance costs in shaa Allah.