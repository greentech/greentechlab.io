---
tags:
  - greentech
  - accomplishsment
  - donate
  - app
  - islam
  - '2020'
  - ramadan
path: 'gtaf-accomplishment-2020'
date: 2021-03-31T18:00:00Z
title: What we have achieved in 2020
subtitle:
  Our efforts and achievements from January 2020 till December 2020 by the
  will of Allah.

topics: ['Report']
isFeatured: true
banner: /images/blog/gtaf-accomplishment-2020.jpg
---

2020 has been a challenging year for many circumstances. But by the grace of The Almighty, we have passed it with some good achievements. We focused largely on team building, foundation and stability. From expanding our team to lowering the bugs in the apps, 2020 has been a pretty busy time here in Greentech Apps Foundation. Nonetheless, in the end, we are more confident and focused on achieving our future goals by the will of Allah. There is still a lot to improve and a lot more to give to the community.

## Key Events

### Team building and process formation

Since the start around 2015 we have been working on building the products and providing the users with the best set of features and best possible experience. Now that we have a large user base to support, we decided to scale the team up with a vision to achieve a bigger goal **“Meeting Ummah’s Digital Need”.**

Before April 2020 we had only one full time and one part time along with the core members and volunteers. To keep up with the maintenance of the applications and to achieve the bigger vision of our organization, we decided to scale up.

We recruited 5 more people to work full time and 3 more to work part time in the projects.

We also have started to introduce Agile processes to make our team robust, using social platforms to reach more people, and a ticketing system for best customer service.

### Support more platforms

We had been focusing on the Android platform for our applications with having 7 apps in Google Play Store but only 2 for the iOS platform. But because of the quality of the apps and growth of the users, the demand for availability in the App Store was increasing day by day. That is why we decided to make more of our apps available in the App Store with the same quality and features set.

In 2020 we released **2 more apps in the App Store for iOS and 1 app for macOS**.

At year end we had our apps in **22,000 devices** installed.

For App Store performance, the top country was Malaysia followed by the United States and United Kingdom.

### Improving user experience

We focused on lowering the crash rates, solving the UX issues to improve the user experience further and keep up with the modern UI trends.

Our average daily crashes were reduced from around **2800 to 1060.**

![](https://lh6.googleusercontent.com/piG7HwrKm1qYOMJQirqIqLLubsxYqyqKEnxWpQ3R2pro0WPFs-ezbnFC-jbHRRjz2THA32Ytv1KSTroBSVMxN56L3RaIYsysIma-aJTdMYpdsw1g5vPIBLI70OfMa4AoOjzqxZ9- 'Chart')

### Growing more and more

We released a whole new app for Quran Memorization with loads of features and neat user interface.

At the year-end, our apps were installed on more than **1 million** devices.

![](https://lh4.googleusercontent.com/SXL5QA-HU8idyA6UWygshtkzbo2ZZLRmmt6RaEMMJsh_c_y3p6QlW645D6BiSKIeL-ivdr1AWYYjdq7c_KBAxqTJoEeLaI5kbNmmfBXFn7knHwD2ifHYh0blJpVvPO-vvVTQONYE 'Chart')

Our apps are available throughout the world. The top 10 countries are:

Bangladesh

Indonesia

India

Malaysia

Nigeria

United Kingdom

USA

Saudi Arabia

Singapore

![](https://lh3.googleusercontent.com/G34w1jX5dCLQeqTT7KTdNqbuUkFsznjSWRVGJILRZTs8AmF2ebrrY_IlSfrAfHqeLYxIeTE2FJbyz93j0meHMieOQNKp3J05T91G-XvaFZtd8dZb0TodpmpUNvtXh8q_vYk8XZws 'Chart')

Active installs per country

## Our Goals in 2021

### Improving User experience

User experience is always our top priority. Since our applications have so many features and functionalities, we plan to revolutionize some of the UI components and core user flows this year.

### Better support system and exposure

We plan to improve our current user support system by hearing more from them and organising the process. Also, we would like to do campaigns to reach out to as many people as we can

### User Profile, analytics and Progress Tracking

We will integrate profile features in many of our applications. So that users can sign in, save their bookmarks and notes and sync with their other devices. Also, this will enable us to implement community-based features in the future.

The analytics dashboard is a very useful tool for our daily used applications. How much time we spend on each part, and how we can build a habit out of it can be improved a lot by viewing statistics and progress. That is why we are planning to integrate analytics features in our application.

### Reaching More Platforms

We will also continue making our apps available on more and more platforms such as iOS, macOS, windows along with modern browsers.
