---
tags: []
path: 8-work-from-home-tips
date: 2021-11-01T00:00:00+06:00
title: 8 work from home tips - Muslim perspective
subtitle: ''
topics:
- productivity
isFeatured: true
banner: "/media/wouter-yvekwjsbziy-unsplash.jpg"

---
Since 2015 I have been working in a hybrid model, 2 days working at the office, 3 days from home. Then, from 2016 onwards, I have been working almost exclusively from home, only going to the office for weekly meetings.

Other than that, I have always had a setup at home that facilitated my work from home. Usually, my home office has always been more comfortable. I have the flexibility to wear what I feel comfortable with, sit as I prefer, control the temperature of the room and have a nap when convenient.

Point to be noted, the following tips are not one size fits all. Please feel free to adapt these tips according to your circumstances.

While working from home, I have always tried to maintain certain habits that I try not to break at all.

Sleep early - as soon as possible after Isha, as per sunnah.

Wake up early - go through morning rituals, pray fajr in masjid followed by quran and zikr.

Then, start work. Usually, it takes me 1 hour to complete my morning rituals.

From fajr to juhr that’s the best time to do deep work, high productivity, high-quality work. Around 1 - 2 hours before juhr, I usually take a break, have a nap (qailula), eat lunch if not fasting, then go and pray juhr in masjid. I have a very light lunch, for example, today, I had a sandwich. Please try to avoid heavy lunch on a working day. Eating too much makes you feel lethargic for the rest of the day.

I try to schedule all my meetings and admin work between juhr and asr, I feel I am not productive at this time, and meetings or admin work are usually the least productive part of work. When it’s asr time, go and pray in the masjid. By this time, I am usually done with all work for the day.

After asr do zikr, go out for a walk or bike ride (exercise time), pray magrib in masjid and return home, spend time with family, help prepare dinner, have dinner before isha and then pray isha in masjid. After isha, it’s sleep time.

Basically:

* ensure you have enough sleep
* ensure you have enough spiritual activities
* ensure you pray in the masjid
* ensure you spend time doing some exercise
* do deep work between fajr and juhr
* leave juhr to asr for less productive activities such as admin work and meetings
* pray and seek Allah’s help to accomplish the above
* ensure dedicated time for family

## Sync vs async

Working from home is not the same as working in the office. Working from home is very asynchronous and if you want to make it synchronous, similar to working at the office, it’s not going to work. So, make sure you do your own work async, but communications can be async or sync, depending on the need. If you need to do sync work, then schedule a slot with your colleague for such. Do not expect them to be available all the time.

One of the best parts of working from home is the flexibility, I am a morning person, doesn’t mean others will be the same. I know some people who prefer to work at night and sleep after fajr. So, keeping meetings after lunchtime works well for both of us. My manager is a night owl, you won’t get hold of him until 12noon. But still, we are working around each other’s preferences. I have received emails from him at 2 am, but he knows I am sleeping, and he knows I will probably reply to him at 6 am when he is asleep.

The bottom line, not everyone is the same, we as individuals have preferences, so understanding these preferences and working around them will make your work/team more cohesive.

_Lessons from one of our Trustees._