---
tags:
  - greentech
  - privacy
  - data
  - '2021'
path: 'we-care-about-your-privacy'
date: 2020-11-17T4:48:00Z
title: We care about your privacy
subtitle:
  Our efforts and achievements from January 2020 till December 2020 by the
  will of Allah.

topics: ['Report']
isFeatured: true
banner: /images/blog/we-care-about-your-privacy.jpg
---

We would like to reassure all our users, that Greentech Apps Foundation is a registered charity, and the only source of funding is your generous donations. And Allah is our witness.

We care for your privacy, for example, when you are using our Al Quran app, we do not collect any location data or any personally identifiable data. Except if you opt in to use our bookmarks sync feature then we use your email address only to link your bookmarks to our secure servers.

Moreover, when you make a donation via [gtaf.org/donate](/donate) we store your personal information securely for legal reasons and in compliance with GDPR. For example, if you opt in for GiftAid, your information can be used to claim GiftAid from HMRC. N.B. We do not have access to your card details.

Greentech Apps Foundation hopes that this brief notice explains how we use your personal information. For more details on our Privacy Policy, please visit [gtaf.org/privacy-policy](/privacy-policy). If you have any concern or complaints, please do not hesitate to contact us at [support@gtaf.org](mailto::support@gtaf.org)
