---
tags:
- greentech
- bookmark
- quran
path: bookmark-redesigned-al-quran
date: 2021-10-14T15:00:00+06:00
title: Quran Bookmark Redesigned
subtitle: Al Quran (Tafsir & by Word)
topics:
- App Update
isFeatured: true
banner: "/media/quran-bookmark.jpg"

---
## Introduction

Assalamu ‘alaykum wa rahmatullah,

Recently we have redesigned our bookmark feature and wanted to share with our users how things work.

We have been contemplating upgrading our bookmark section for a while.

We went through a lot of requests, suggestions from our users before we started our redesigning process. Furthermore, we also carried out surveys, tests, and interviews to understand more about our user’s needs.

Throughout the research, we realized many different ways everyone uses our bookmark. Some use it for research purposes, while others for keeping track of their recitation.

After brainstorming, we did our best to come up with a better solution.

## Earlier Bookmark System

In our previous bookmark system, we had **Multiple Bookmarks** & **Single Bookmarks**.

Multiple bookmarks allowed our users to have a large number of bookmarks in one folder. In comparison, a single bookmark allowed our users only to have one bookmark.

After doing some survey, we realized that many of our users were not probably understanding this feature. And having a similar name wasn’t helping.

As a result, based on the research, we decided to first rename our bookmarks folders into **Collections** & **Pins.**

![Library Section - Collection, Pins & Notes ](/media/collection-pins-notes.png "Figure: Library Section - Collection, Pins & Notes ")

**Collections** - Collections are similar to multiple bookmarks, where you can save a large number of bookmarks in one folder. It is useful for gathering multiple ayat under a category like Zakat, Salat.

**Pins** - We renamed the single bookmark as pins. A pin can only contain one saved ayah.

And whenever you save a new ayah in the pin, it replaces the old ayah. It is useful for tracking recitation progress.

## Save Ayah to Collections / Pins

We tried to make the ayah bookmarking process more user-friendly and clear. So we updated the UI and its behaviour.

Previously it came from the top, which was slightly difficult for our users to reach, and it had very little information.

We moved the section to the bottom, so you can reach the options easily. We also added more details to help understand more about collections & pins.

The process of saving an ayah is quite similar to our previous design.

You need to press on the **3 dot icon**. And then choose to **add a bookmark.** From there, you can create a new Collection / Pin Folder or just save them to an existing folder.

<img src="/media/01-saveayahdialogue.gif" width="225" alt="Figure - Saving an ayah" title="Figure - Saving an ayah">

## Library Section

As the main purpose of collections, pins and notes are to study, so we brought all those features in one place and renamed them to Library. Keeping the section more organized.

You can access each category by pressing on the respective tab.

<img src="/media/02-library.gif" width="225" alt="Figure - Library Section" title="Figure - Library Section">

## How do Collections work?

![Figure - Collections](/media/library-1.png "Figure - Collections")

We have made some slight modifications to how the bookmark collection looks. We have observed that a lot of features were conflicting with each other in the previous bookmark.

Swiping to remove at times causes the section to change. We also felt that expanding and collapsing the bookmark folders wasn’t very intuitive if there were many bookmark folders.

Many users were also accidentally moving the bookmark from one folder to another.

So we brainstormed and decided to come up with a few changes. We moved from expanding and collapsing UI to putting the bookmarked ayah inside the folder (collections).

So instead of collapsing and expanding, you can now enter a folder to see all the ayahs inside that collection.

This allowed us to reduce some of the mistakes and also add sorting options for collections and individual ayah. It’s discussed in more detail below.

### **Search**

One of the first things we decided to do was add a search bar. You will find the search bar in all the 3 sections. This feature will be rolled out in phases.

You can use it to find a collection easily by searching by its name. Now you don’t have to worry about scrolling through multiple bookmarks folders to find the suitable one.

<img src="/media/03-collectionfoldersearch.gif" width="225" alt="Figure - Search" title="Figure - Search">

### **Edit/Delete a Collection Folder**

We have improved the UI of the Edit/Delete option. You can access it by pressing the 3 dot option at the right of your collection.

From there, you can edit names, change colour or delete the folder with all the bookmarks inside. Be careful with deleting a collection, as it will delete all the ayahs inside.

<img src="/media/04-collectionedit.gif" width="225" alt="Figure - Edit Collection Folder" title="Figure - Edit Collection Folder">

### **Sort**

You will now have the option to sort the collections in multiple ways. You have the option to sort by A-Z based on the collection folder name, creation date, last modified date, and even in your own custom way by using the drag and drop option.

<img src="/media/05-collectionsort.gif" width="225" alt="Figure - Sort Collection Folder" title="Figure - Sort Collection Folder">

### **Inside the Collection Folder**

You can tap on a collection and access all the ayats you saved inside that folder.

You can read the ayahs from here. You can also go to the sura just by tapping on the ayah.

<img src="/media/06-insidecollection-scrolling.gif" width="225" alt="Figure - Inside Collection" title="Figure - Inside Collection">

### **Sort Verses inside a collection folder**

As we have moved the ayahs in the folder, it made it easy for us to add the sort option in the collection folder as well.

You can now sort the ayahs by Sura number & verse, date or use the drag & drop option to arrange it any way you like.

### **More verse option**

The 3 dot option at the top right corner of each verse will give you more options. You can delete, copy, share and do many more things just by tapping the option.

<img src="/media/07-insidecollection-moreoption.gif" width="225" alt="Figure - More verse option" title="Figure - More verse option">

## How do Pins work?

![Figure - Pins](/media/pin.png)

Pins are single bookmarks where you can only save one ayah at a time. Saving another ayah will replace the previously saved ayah. It can be used to track recitation progress.

You can tap on a pin and go to that ayah.

### **Edit a Pin folder**

To edit a pin, you need to tap in the 3 dot option icon at the right of a pin.  
From there, you can rename the folder, change colours and delete the pin.

### **Search**

You can search for a pin by its name using the search option.

### **Updating ayah inside a pin**

If you save an ayah in an existing pin, it will replace the old ayah with the new one. So that you can easily track recitation progress.

You will also get notified by a message. You will have a few seconds to undo your action.

### **Sort**

We have also added the sort option in the pins and you will find similar ways of sorting as the collections folder.

## How do Notes work?

![Figure - Notes](/media/notes.png)

Notes are now included in the library section. You can now access all your study material from one place.

You will find all your notes arranged by their Sura name.

You can press on a sura and go through all your notes under it. If you tap on a note, it will take you to the tafsir view of that ayah.

### **Search**

From the notes section, you can search the content of the notes.

### **Edit a note from Tafsir View**

You can tap on a note and go to the tafsir view to edit the note there.

### **Delete Notes**

You can easily delete a note by just tapping and holding the notes.

## Export/Import Library

![Figure - Export/Import Library](/media/collection-pin-export-import.png)

We have included the option to import or export the whole library data so that you can store a backup of the library data.

Additionally, if you want to share your library contents with others, you can easily share your exported files with them.

In the future, we have plans to make this more intuitive in sha Allah.

## Conclusion

The update has been very challenging work for us and we had to spend a lot of time going through all the requests of our users, technological constraints, and many more things before deciding to do something.

But at the end of the day, we are humans and sometimes it becomes difficult to reconcile with everyone’s request.

So if there is anything you don’t like in the new update, we humbly apologize.

We won’t be finishing our work here. We will be continuously updating the app based on new requests and suggestions.

And you can **help us** by letting us know what new features worked for you and **what we can do to make it better** for you!

You can participate in this [Google Form Survey](https://docs.google.com/forms/d/e/1FAIpQLSdf6GZ_UE4JVbeLJDuBjcCTi125ZnwG-Zsc_Bvka7jWF7xPyw/viewform) to share with us your valuable feedback. We will use your feedback to improve our bookmark section further so that it’s easier for you.

Our goal is to serve Allah together by creating digital products that help us come closer to Allah.

**_Disclaimer:_**

_Search, sort features are currently available on the iOS version only. Soon it will be added to the Android version too, in sha Allah._