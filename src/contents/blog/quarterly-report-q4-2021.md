---
tags: []
path: quarterly-report-q4-2021
date: 2022-01-26T03:00:00.000+06:00
title: Quarterly Report Q4 2021
subtitle: ''
topics:
- Report
isFeatured: true
banner: "/media/rsz_1jungwoo-hong-cyumacqmyvi-unsplash.jpg"

---
Bismillahir rahmanir rahim.

Assalamu ‘alaykum!

May Allah reward you for your continuous support. We wanted to share a few notable achievements in the last quarter,

1. Quran app release with a redesigned bookmark with search, sort and many improvements
2. Hadith app release with several content corrections and book additions
3. Preparing English Quiz contents for the Deen Quiz app
4. Working on bringing Bangla Audio Translation to the Quran app
5. Commenced designing a brand-new Seerah app

We wanted to thank you for your endless support and valuable contributions!

Regards,  
[GTAF.org](https://gtaf.org/) Team