---
tags:
  - '2022'
  - '2021'
  - islam
  - app
  - donate
  - accomplishment
  - greentech
path: gtaf-accomplishment-2021
date: 2022-08-22T00:00:00.000+06:00
title: Our Achievements of 2021
subtitle: ''
topics:
  - Report
isFeatured: true
banner: '/media/cristian-grecu-6ybaqeenrou-unsplash.jpg'
---

With a solid team in place, **2021** was the time for growth. We have implemented a number of long-requested features and solved some of the biggest complaints from our users. At the same time, we’ve increased our exposure with a new and improved website with a better support page, utilized our own backend infrastructure for the sync mechanism of user data, and made our app available for more platforms!

## Better User Experience

We always put the user’s experience at the top of our priority list. And with a lot of valued users supporting our [Quran app](https://gtaf.org/apps/quran), we sought to have significant improvements in one or more features.

After doing research and surveys, we settled on the [Library (bookmarks and notes)](https://gtaf.org/blog/bookmark-redesigned-al-quran) and the Planner modules of the Quran app to be improved. We did heavy research and a lot of work to make the user flows smoother, less complicated and better looking. We had to consider both the new and existing users in mind.

We also included offline features and English translation to our existing [Quiz app](https://gtaf.org/apps/quiz) after a lot of requests.

## Better Support System and Exposure

### Website Revamp

To give a better user experience in the products, you’ll need to listen to your users. With that in mind, [we revamped our existing website](https://gtaf.org/blog/website-revamped). The Help & Support page and the contact pages were the two most important factors for the redesign.

We also improved the look and feel of the website, integrated better SEO and also faster performance. At the same time, we upgraded the donation pages for users to find related information faster before contributing.

### Announcement Section

The apps now have a dedicated announcement section so that they can be in touch with our latest works and news.

## User Profile and Library Syncing

We have built a profile system into our Quran application. The users will be able to sign up, and sync their bookmarks and notes and not worry about losing them.

Users can also have the data synced between multiple devices at the same time.

We have built our backend server infrastructure to allow more community-engaging features in the coming days.

## Reaching More Platforms

To facilitate our goal of reaching more platforms, we have deployed our[ Quran app on macOS](https://apps.apple.com/us/app/al-quran-tafsir-by-word/id1437038111)!

Also, we have released our Hadith collection app for iOS and macOS users with full functionality. Previously it had limited features.

## Other Notable Mentions

We have added a number of new translations of the Quran (of different languages) and audio recitations from new Reciters.

We also have added the “Compare Isnad (chains of narration)” feature in the Hadith Collection app.

## Some Statistical Milestones

### Time Spent on Our Apps

Our biggest discovery was how much the users have spent using our app over the last 4 years (April 2018 - April 2022).

During this period, all our users have spent a cumulative amount of **over 5000 years** using our apps.

And in 2021 alone, the users have spent around **1810 years** of time using our application.

### Active Users

And at the year-end, our apps were installed on more than **1.4 million** devices \*.

![](https://lh5.googleusercontent.com/jAoQhVGuQWqMfM7dlkz6fwCSRX_LYnq12APW74JZoMZcDnkV16Wqyl1bzfCTcymu2pqbOhu41OBwOdp4OoXTrWm26eZ4Bdo2suk6IMuFnSkELGHCW-WJClB1Mm6vr3Caure-WAGUkTeLiWgXhPGHKr8)

Cumulative active installs for all our apps (Android platform only)

### Top Countries

Our apps are available throughout the world. The top 10 countries are:

- Bangladesh
- Indonesia
- India
- Malaysia
- Nigeria
- United Kingdom
- Saudi Arabia
- USA
- Singapore

![](https://lh4.googleusercontent.com/QVPueordT-shJvMW7lmw8MOiKwRqGzee1ejmpYjcJjyLQfiY9RY4KcADTAH6DpVqvQ9atPGnY7G6S-OoUiWWrMM72qwGSVDFf9et5efA_kwqplUQ9a8fEvk2tSsKPbBkoiiN1X78I4aYoWGlR-BncW4)

Active installs for all our apps per country (Android platform only)

# Our Goals in 2022

## Being more organised

We will keep improving our organisational structure and processing to be long-lasting. The focus will be on having a visual scorecard for our KPIs. This will enable us to keep a bird's eye view of the key metrics.

We will have training modules for staff, individual career ladders, and hackathons to increase engagement in the coming months.

## Hearing more from the users and solving their problems

You can only solve the problems you are aware of and the best people to find the problems of the products are the users of them. We are set to incorporate Canny to give the users a platform to share their problems and ideas.

We will also utilize Chatwoot to provide better user support.

## User stats for more engagement

Our Quran app will have a new feature module related to statistics. When a user sees how much s/he has engaged with the product and how much they have benefited from it., they will feel motivated to utilise the application even more. This will help them create a learning habit.

## Solving new problems

This year we are set to bring a brand new Seerah (history) application to the people. At the same time, we’re planning to tackle some new sectors of problems, e.g.

- Quran-Islam web search / root search / all search
- Kids learning app
- Auto extracting word-by-word audios for a new reciter
- Hadith topic modelling for better search
- Extract people's names from Hadith to create a connected database

Footnotes:

\* If a person has multiple of our apps installed on his/her device, it’s counted as such.
