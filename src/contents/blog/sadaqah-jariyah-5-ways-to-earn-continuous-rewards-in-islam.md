---
tags: []
path: sadaqah-jariyah-5-ways-to-earn-continuous-rewards-in-islam
date: 2022-03-14T12:00:00.000+06:00
title: Sadaqah Jariyah | 5 ways to earn continuous rewards in Islam
subtitle: ''
topics: []
isFeatured: true
banner: "/media/rsz_josh-appel-netpasr-bmq-unsplash.jpg"

---
### **What is Sadaqah Jariyah?**

Sadaqah Jariyah means a continuous and ongoing charity. It is one of the most rewarding acts a person can do in their lives as the benefits of giving this type of charity can be reaped in this lifetime and even after one has passed. Indeed, the men who practice charity and the women who practice charity and \[they who\] have loaned Allah a goodly loan - it will be multiplied for them, and they will have a noble reward. \[Surah Al-Hadid; Verse 18\]

### **What is the difference between Sadaqah and Sadaqah Jariyah?**

Sadaqah is short-term and benefits the recipients on a single occasion. It counts as one good deed for the giver. Sadaqah Jariyah, on the other hand, is long-term and benefits the recipients continuously, and the giver continues to be rewarded even after death.

For example, providing food for someone is Sadaqah as it benefits that person momentarily. But building a masjid for people where they can regularly go to pray is Sadaqah Jariyah as it aids people for years to come. In turn, it will continue to benefit the giver in this life and the Hereafter in shaa Allah.

### **Why is Sadaqah Jariyah important in Islam?**

Allah (SWT) has guaranteed to record such continuous acts of charity in the following verse:

"We shall certainly bring the dead back to life and record what they did as well as what they had left behind. We keep an account of everything in a clear Record." \[Surah Ya-Sin; Verse 12\]

It is also mentioned in a Hadith that:

The Messenger of Allah (ﷺ) said, "When a man dies, his deeds come to an end, except for three: A continuous charity, knowledge by which people derive benefit, a pious son who prays for him." \[Muslim\]

Therefore, when you give Sadaqah Jariyah in your name or the name of a loved one, the blessings continue even after death.

The rewards of Sadaqah Jariyah are immense and endless. Let us all try to be part of such acts of charity in shaa Allah.

#### **5 easy ways to earn continuous rewards in Islam**

1. Sponsor a child or a orphan
2. Giving Dawah or spreading beneficial knowledge
3. Take part in building a masjid, school or a hospital
4. Share copies of the Quran
5. Donate as little as a dime to any Sadaqah Jariyah projects like ours at Greentech**

May Allah SWT reward us all for our good deeds and forgive our shortcomings, and may He grant us all Jannatul Ferdous. Ameen.

Don’t forget to be a part of our cause, and support our projects to help us achieve our goals in shaa Allah. Your contributions will encourage Islamic education and fulfill the Islamic digital needs worldwide in shaa Allah.

To become a monthly donor, please visit [gtaf.org/donate](https://gtaf.org "gtaf.org/donate")