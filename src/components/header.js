import { Link } from 'gatsby';
import React, { useEffect, useState } from 'react';
import NavMenu from './NavMenu';
import { SearchModal } from './searchModal';
import BrandLogo from '../../static/images/logo-wide.svg';
import { Menu, SearchIcon } from '../custom-icons';

function Header() {
  const [isExpanded, toggleExpansion] = useState(false);
  const [isSearchOpen, setSearchOpen] = useState(false);
  const [hasScrolled, setHasScrolled] = useState(false);

  useEffect(() => {
    setHasScrolled(window.scrollY > 16);
    const handleScroll = () => {
      setHasScrolled(window.scrollY > 16);
    };
    window.addEventListener('scroll', handleScroll, { passive: true });
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  return (
    <div
      className={`border-b bg-neutral border-neutral-2 fixed z-30 w-full transition-shadow duration-300 ${
        hasScrolled ? 'shadow' : ''
      }`}
    >
      <div className="flex items-center justify-between max-w-6xl p-4 mx-auto whitespace-nowrap">
        <Link to="/">
          <div className="flex items-center w-32 no-underline">
            <img src={BrandLogo} className="mr-2 fill-current" alt="logo" />
            <span className="sr-only">Home</span>
          </div>
        </Link>

        {!isExpanded ? <NavMenu isExpanded={isExpanded} /> : null}

        <div className="flex items-center gap-2">
          <div className="flex items-center gap-2">
            <button
              onClick={() => {
                setSearchOpen(true);
              }}
              className="p-2 rounded-full cursor-pointer text-neutral-8 hover:bg-neutral-2"
            >
              <SearchIcon />
              <span className="sr-only">search</span>
            </button>
            <SearchModal
              open={isSearchOpen}
              onClose={() => {
                setSearchOpen(false);
              }}
            />
            <Link to="/donate">
              <button className="block w-20 px-2 py-2 text-sm font-bold rounded-full bg-secondary hover:bg-secondary-0 text-neutral lg:px-6 lg:py-3 lg:w-28 lg:text-base">
                Donate
              </button>
            </Link>
            <button
              className="items-center block px-3 py-2 border-white rounded lg:hidden"
              onClick={() => toggleExpansion(!isExpanded)}
            >
              <Menu />
              <span className="sr-only">menu</span>
            </button>
          </div>
        </div>
      </div>
      {isExpanded ? <NavMenu isExpanded={isExpanded} /> : null}
    </div>
  );
}

export default Header;
