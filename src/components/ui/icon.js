import { cloneElement } from 'react';
import { cn } from '../../utils';

const Icon = ({ icon, className }) => {
  const iconClassName = cn(className, 'text-2xl');
  return cloneElement(icon, { className: iconClassName });
};

export default Icon;
