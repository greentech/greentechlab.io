import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Modal } from './modal';
import algoliasearch from 'algoliasearch';
import algoliaImage from '../../static/images/search-by-algolia-light-background.png';
import { Link } from 'gatsby';

export const SearchModal = ({ open, onClose }) => {
  const [searchHits, setSearchHits] = useState(null);
  const [searchText, setSearchText] = useState('');

  function search(query) {
    const client = algoliasearch(
      'BYMMFR7J73',
      '673b724783fc55d997403fc3debb67dd',
    );
    const index = client.initIndex('gtaf_website_pages');

    // only query string
    return index.search(query);
  }

  const _onClose = () => {
    setSearchHits(null);
    setSearchText('');
    onClose && onClose();
  };

  return (
    <Modal isOpen={open} onClose={_onClose} className="max-w-sm">
      <div className="max-w-md">
        <div className="rounded-lg shadow-xl bg-neutral-0">
          <div className="flex">
            <form
              onSubmit={(e) => {
                e.preventDefault();
                let q = searchText;
                if (q.trim().length == 0) {
                  setSearchHits(null);
                } else {
                  search(q).then(({ hits }) => {
                    setSearchHits(hits);
                  });
                }
              }}
            >
              <input
                type="search"
                placeholder="Find what you need..."
                className="w-full px-4 py-2 border-transparent rounded-lg focus:border-transparent focus:ring-0 font-regular placeholder-neutral-5"
                value={searchText}
                onChange={(e) => {
                  setSearchText(e.target.value);
                }}
              />
              <p className="px-4 py-2 text-xs text-neutral-7">
                Press <code>Enter</code> to search
              </p>
            </form>
          </div>
          {searchHits && (
            <div className="p-4 pt-0">
              <div className="overflow-y-auto max-h-72">
                {searchHits.length == 0 && (
                  <div className="my-4 support-query-item text-accent-2">
                    No items found
                  </div>
                )}
                {searchHits.map((item) => (
                  <div
                    key={item.objectID}
                    className="my-4 support-query-item text-accent-2"
                  >
                    <Link
                      className="link-underline-accent"
                      to={item.path}
                      dangerouslySetInnerHTML={{
                        __html: item._highlightResult.title.value,
                      }}
                    ></Link>
                  </div>
                ))}
              </div>

              <img
                src={algoliaImage}
                className="h-4 mt-2 ml-auto"
                alt="Algolia search"
              />
            </div>
          )}
        </div>
      </div>
    </Modal>
  );
};

SearchModal.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
};
