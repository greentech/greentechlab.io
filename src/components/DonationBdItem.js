import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { copyToClipboard } from '../utils';
import { useToast } from '../hooks/useToast';
import Icon from './ui/icon';
import { ChevronDown } from '../custom-icons';
import { motion } from 'framer-motion';
export function DonationBdMedium({
  selectedPaymentMethod,
  method,
  isCopied,
  setSelectedPaymentMethod,
  setShowQR,
  setIsCopied,
}) {
  const [isOpen, setIsOpen] = useState(false);
  const handleIconClick = (paymentMethod) => {
    if (selectedPaymentMethod != null) {
      if (paymentMethod && paymentMethod.name === selectedPaymentMethod.name) {
        setSelectedPaymentMethod(null);
      } else {
        setSelectedPaymentMethod(paymentMethod);
      }
    } else {
      setSelectedPaymentMethod(paymentMethod);
    }
    setShowQR(false);
    setIsCopied(false);
    setIsOpen(!isOpen);
  };
  const { addToast } = useToast();
  const handleCopyLink = async (text) => {
    setIsCopied(true);
    copyToClipboard(text, addToast);
  };
  console.log(selectedPaymentMethod?.name === 'nagad');
  return (
    <div className="">
      <div key={method.name}>
        {method.name == 'bkash' && (
          <p className="mt-3 mb-4 font-kalpurush lg:hidden lg:mt-0">
            সংগত কারণে আমরা বিকাশ ব্যবহার অনুৎসাহিত করছি, তারপরও এমন কেউ যদি
            থাকেন যার বিকাশ ছাড়া অন্য অপশন নেই, তাহলে বিকাশে পাঠাতে পারেন।
          </p>
        )}

        <div
          key={method.name}
          onClick={() => handleIconClick(method)}
          className={`h-24 lg:h-44 rounded-2xl flex items-center justify-between lg:justify-center cursor-pointer transition-all
            ${
              selectedPaymentMethod?.name === method.name
                ? ' border-primary bg-primary-0 border-2'
                : 'border border-neutral-2 bg-neutral-0 hover:border-primary '
            }`}
        >
          {method.name === 'aibl' && (
            <div className="px-4 lg:px-0 w-auto">
              <Icon
                className="w-auto h-8 px-2 md:w-full md:h-full lg:hidden" // icon3 for small screens
                icon={method.icon3}
              />
              <Icon
                className="hidden lg:block w-auto h-10 px-2 md:w-full md:h-full" // logo for lg screens
                icon={method.logo}
              />
            </div>
          )}
          {method.name != 'aibl' && (
            <div key={`${method.name}-small`} className="px-4 lg:px-0 w-auto  ">
              <Icon
                className="w-auto h-10 px-2 md:w-full md:h-full"
                icon={method.logo}
              />
            </div>
          )}
          <div className="">
            <motion.div
              animate={{
                rotate:
                  selectedPaymentMethod?.name != method.name
                    ? 0
                    : selectedPaymentMethod?.name === method.name
                    ? 180
                    : 0,
              }}
              transition={{ duration: 0.3 }}
              className="h-10 w-10 rounded-full flex items-center justify-center bg-neutral-1 mx-6 lg:hidden lg:w-auto lg:h-auto lg:mx-0"
            >
              <ChevronDown className="w-6 h-6" />
            </motion.div>
          </div>
        </div>

        {selectedPaymentMethod?.name === method.name && (
          <div className="flex flex-col px-6 pt-6 pb-6 mt-4 lg:hidden rounded-2xl bg-neutral-0 lg:flex ">
            <p className="max-w-36 font-anek-bangla text-xl leading-[1.6] font-semibold inline-block">
              <span className="border-b border-neutral-2">
                {selectedPaymentMethod.title_bn}
              </span>
            </p>

            <div
              className={`${selectedPaymentMethod.description_font} mt-4 inline-block text-lg leading-[1.78] tracking-[0.2px]`}
              dangerouslySetInnerHTML={{
                __html: selectedPaymentMethod.description_bn_small,
              }}
            />
            {selectedPaymentMethod.description_bn_last_line && (
              <div className="flex items-center">
                <p className="font-kalpurush text-lg leading-[1.78] tracking-[0.2px]">
                  <b>{selectedPaymentMethod.description_bn_last_line_small}</b>
                </p>
                {!isCopied ? (
                  <button
                    onClick={() => handleCopyLink(selectedPaymentMethod.number)}
                    className="shadow-none hover:shadow-lg inline-flex items-center px-2 ml-2 border rounded-lg t-4 bg-neutral-1 border-ash-4"
                  >
                    <img src="/icons/copy-icon.svg" className="my-1" />
                    <p className="my-1 ml-1">কপি করুন </p>
                  </button>
                ) : (
                  <button
                    onClick={() => handleCopyLink(selectedPaymentMethod.number)}
                    className="shadow-none hover:shadow-lg inline-flex items-center px-2 ml-2 border rounded-lg t-4 bg-neutral-1 border-ash-4"
                  >
                    <img src="/icons/tik-icon.svg" className="my-1" />
                    <p className="my-1 ml-1">কপি হয়েছে </p>
                  </button>
                )}
              </div>
            )}
            {selectedPaymentMethod.name == 'bkash' && (
              <div className="w-full h-full mt-6">
                <div className="grid w-full h-16 grid-cols-2 gap-x-2 ">
                  <a
                    href="https://shop.bkash.com/greentech-apps-foundation01914/pay/bdt100/nPiZET"
                    target="_blank"
                    rel="nofollow noreferrer"
                  >
                    <p className="shadow-none hover:shadow-lg h-full text-xl lg:text-2xl tracking-[0.2px] font-kalpurush border rounded-lg border-neutral-5 flex items-center justify-center ">
                      ১০০ টাকা
                    </p>
                  </a>
                  <a
                    href="https://shop.bkash.com/greentech-apps-foundation01914/pay/bdt500/z5xrnN"
                    target="_blank"
                    rel="nofollow noreferrer"
                  >
                    <p className="shadow-none hover:shadow-lg h-full text-xl lg:text-2xl tracking-[0.2px] font-kalpurush border rounded-lg border-neutral-5 flex items-center justify-center ">
                      ৫০০ টাকা
                    </p>
                  </a>
                </div>
                <div className="grid w-full h-16 grid-cols-2 mt-3 gap-x-2">
                  <a
                    href="https://shop.bkash.com/greentech-apps-foundation01914/pay/bdt1000/faD2nc"
                    target="_blank"
                    rel="nofollow noreferrer"
                  >
                    <p className="shadow-none hover:shadow-lg h-full text-lg lg:text-2xl tracking-[0.2px] font-kalpurush border rounded-lg border-neutral-5 flex items-center justify-center ">
                      ১০০০ টাকা
                    </p>
                  </a>
                  <a
                    href="https://shop.bkash.com/greentech-apps-foundation01914/paymentlink"
                    target="_blank"
                    rel="nofollow noreferrer"
                  >
                    <p className="shadow-none hover:shadow-lg h-full text-xl lg:text-2xl tracking-[0.2px] font-kalpurush border rounded-lg border-neutral-5 flex items-center justify-center">
                      অন্য এমাউন্ট
                    </p>
                  </a>
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

DonationBdMedium.propTypes = {
  method: PropTypes.object.isRequired,
  isCopied: PropTypes.bool.isRequired,
  selectedPaymentMethod: PropTypes.object.isRequired,
  setSelectedPaymentMethod: PropTypes.func.isRequired,
  setShowQR: PropTypes.func.isRequired,
  setIsCopied: PropTypes.func.isRequired,
};
