import React from 'react';
import { useStaticQuery, graphql, Link } from 'gatsby';
import PropTypes from 'prop-types';
import BoldTitle from './BoldTitle';
import { Image } from './image';

const query = graphql`
  {
    allAppList(limit: 4) {
      nodes {
        appName
        route
        smallImageFile {
          childImageSharp {
            gatsbyImageData(width: 700, placeholder: NONE, layout: CONSTRAINED)
          }
        }
      }
    }
  }
`;

function OtherApps({ currentAppName }) {
  const data = useStaticQuery(query);
  const appList = data.allAppList.nodes.filter(
    (item) => item.appName !== currentAppName,
  );

  if (appList.length === 4) {
    appList.pop();
  }

  return (
    <div className="bg-neutral md:flex md:justify-center">
      <div className="max-w-6xl p-6 my-12 md:my-24 ">
        <BoldTitle>Check Out Our Other Apps</BoldTitle>
        <div className="mt-10 md:grid md:grid-cols-3 md:gap-6 lg:gap-11">
          {appList.map((item) => (
            <Link to={`/apps/${item.route}`} key={item.route}>
              <div className="m-6 transition duration-500 ease-in-out md:max-w-80 rounded-2xl hover:shadow-2xl md:m-0">
                <div className="bg-primary-0">
                  <Image imageFile={item.smallImageFile} className="m-auto" />
                </div>
                <div className="p-6 border-2 font-semiBold rounded-bl-2xl rounded-br-2xl border-primary-0">
                  {item.appName}
                </div>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
}

OtherApps.propTypes = {
  currentAppName: PropTypes.string.isRequired,
};

export default OtherApps;
