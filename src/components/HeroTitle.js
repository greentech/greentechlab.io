import React from 'react';
import PropTypes from 'prop-types';
import { cn } from '../utils';

function HeroTitle({ text, className }) {
  return (
    <h1
      className={cn(
        'font-semiBold text-4xl md:text-6xl text-center md:text-left max-w-3xl my-6 md:leading-tight',
        className,
      )}
      dangerouslySetInnerHTML={{ __html: text }}
    ></h1>
  );
}

HeroTitle.propTypes = {
  text: PropTypes.string.isRequired,
  className: PropTypes.string,
};

HeroTitle.defaultProps = {
  className: '',
};

export default HeroTitle;
