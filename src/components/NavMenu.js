import React, { useContext } from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import Dropdown from './Dropdown';
import { navBarLinks } from '../constants';
import { CommonContext } from '../context';
import { ChevronDown } from '../custom-icons';

function NavMenu({ isExpanded }) {
  const { location } = useContext(CommonContext);

  const isRouteActive = (route) => {
    if (!route) {
      return false;
    }

    if (route === '/' && location.pathname === '/') {
      return true;
    }

    if (typeof route === 'string' && route.startsWith('/blog/category/')) {
      return location.pathname.startsWith('/blog/category/');
    }

    return route !== '/' && location.pathname.includes(route);
  };

  const renderLinkItem = (link) => (
    <Link
      className="inline-flex leading-6 tracking-wide font-regular nav-item"
      to={link.route || location.pathname}
    >
      {link.title}
      {link.menu ? <ChevronDown /> : null}
    </Link>
  );

  const renderLinkDropdown = (link) => (
    <Dropdown
      showExternalLinkIcon
      isResponsive
      component={
        <div className="inline-flex leading-6 tracking-wide cursor-pointer font-regular nav-item">
          {link.title}
          <ChevronDown />
        </div>
      }
      menu={link.menu}
    />
  );

  const activeLinkIndicator = (isDropdown = false) => (
    <div
      className={`absolute w-full -bottom-7 hidden lg:block ${
        isDropdown ? '-left-3' : ''
      }`}
    >
      <div className="w-8 h-1 mx-auto bg-primary rounded-2xl" />
    </div>
  );

  return (
    <nav
      className={`${
        isExpanded ? 'block' : 'hidden'
      } lg:block lg:items-center w-full pl-4 lg:pl-0 pb-4 lg:pb-0 shadow-xl lg:shadow-none`}
    >
      {navBarLinks.map((link) => (
        <div
          className="relative block mt-4 no-underline lg:inline-block lg:mt-0 lg:ml-6"
          key={link.title}
        >
          {link.menu ? renderLinkDropdown(link) : renderLinkItem(link)}

          {link.route && isRouteActive(link.route) && activeLinkIndicator()}

          {link.menu &&
            link.menu.some((item) => item.route && isRouteActive(item.route)) &&
            activeLinkIndicator(true)}
        </div>
      ))}
    </nav>
  );
}

NavMenu.propTypes = {
  isExpanded: PropTypes.bool.isRequired,
};

export default NavMenu;
