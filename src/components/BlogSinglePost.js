import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { GatsbyImage } from 'gatsby-plugin-image';

import { CommonContext } from '../context';
import { useScrollToSection } from '../hooks/useScrollToSection';

import { getHeadings } from '../utils';
import { blogConstants } from '../constants';

import Layout from './layout';
import HeroTitle from './HeroTitle';
import BlogHighlights from './BlogHighlights';
import BlogCategoryBanner from './BlogCategoryBanner';
import SubscribeBlock from './SubscribeBlock';
import BlogSharing from './BlogSharing';
import TableOfContent from './TableOfContent';
import BackToTop from './BackToTop';
import PostContent from './PostContent';
import PostMetadata from './PostMetadata';

const MobileSharing = ({ tocHeadings, canonicalUrl }) => {
  return (
    <div className={tocHeadings.length <= 0 ? '' : 'block md:hidden'}>
      <hr className="w-full my-6 text-neutral-2" />
      <div className="flex flex-col items-center justify-center gap-4">
        <p className="text-xl font-semiBold">Share the Knowledge</p>
        <BlogSharing canonicalUrl={canonicalUrl} />
      </div>
    </div>
  );
};

MobileSharing.propTypes = {
  tocHeadings: PropTypes.array.isRequired,
  canonicalUrl: PropTypes.string.isRequired,
};

const BlogSinglePost = ({ data, location }) => {
  const {
    title,
    featuredImage,
    content,
    modified,
    seo,
    categories,
    postMetadata,
  } = data.wpPost;
  const featuredBlogs = data.allFeaturedWpPost.nodes;
  const canonicalUrl = data.site.siteMetadata.siteUrl + location.pathname;

  const [tocHeadings, setTocHeadings] = useState([]);
  const scrollToSection = useScrollToSection();
  const initialScrollRef = useRef(false);

  useEffect(() => {
    if (content) {
      setTocHeadings(getHeadings(content));
    }
  }, [content]);

  useEffect(() => {
    if (location.hash !== '' && content && !initialScrollRef.current) {
      const timer = setTimeout(() => {
        // This timer is necessary!
        const sectionId = location.hash.replace('#', '').replace('%C2%A7', ''); // Remove the URL-encoded § symbol
        scrollToSection(sectionId);
      }, 0);
      return () => clearTimeout(timer);
    }
  }, [content, location.hash]);

  return (
    <CommonContext.Provider value={{ location }}>
      <Layout
        title={title}
        description={seo.description}
        keywords={seo.focusKeywords || []}
        image={featuredImage.node.mediaItemUrl}
        type={seo.openGraph.type}
        additionalData={seo.openGraph.slackEnhancedData}
        location={location}
      >
        <section
          className={`w-full ${blogConstants.layout.maxWidth} ${blogConstants.layout.padding} m-auto`}
        >
          <PostMetadata
            categories={categories}
            timeToRead={postMetadata?.timeToRead}
            modified={modified}
          />
          <div>
            <HeroTitle
              text={title}
              className="max-w-full my-5 text-3xl font-bold text-start leading-10 md:text-5xl md:leading-[60px] md:my-6"
            />
          </div>

          <div>
            <div
              className="w-full text-sm leading-6 md:leading-7 md:text-base md:p-0 text-neutral-7"
              dangerouslySetInnerHTML={{ __html: postMetadata.summary }}
            />
          </div>
          <div className="block mt-5 lg:hidden">
            <TableOfContent tocHeadings={tocHeadings} />
          </div>
          <div className="my-10">
            <GatsbyImage
              alt={featuredImage.node.altText}
              image={featuredImage.node.gatsbyImage}
              className="w-full"
            />
          </div>
          <PostContent
            content={content}
            tocHeadings={tocHeadings}
            canonicalUrl={canonicalUrl}
          />
          <MobileSharing
            tocHeadings={tocHeadings}
            canonicalUrl={canonicalUrl}
          />
        </section>
        <div className="w-full max-w-6xl px-4 m-auto my-8 mt-16 bg-neutral md:mt-24">
          {categories.nodes.map((category) => {
            if (category?.categoryBannerImages.categoryBannerImage === null)
              return null;
            const {
              bannerLinkUrl,
              categoryBannerImage: { gatsbyImage, altText },
            } = category.categoryBannerImages;

            return (
              <BlogCategoryBanner
                key={category.slug}
                bannerLinkUrl={bannerLinkUrl}
                categoryBannerImage={gatsbyImage}
                altText={altText}
              />
            );
          })}
        </div>
        <BlogHighlights
          title="Explore More Inspiring Reads"
          count={3}
          featuredBlogs={featuredBlogs}
        />
        <SubscribeBlock />
        <BackToTop />
      </Layout>
    </CommonContext.Provider>
  );
};

BlogSinglePost.propTypes = {
  data: PropTypes.shape({
    wpPost: PropTypes.shape({
      title: PropTypes.string.isRequired,
      featuredImage: PropTypes.shape({
        node: PropTypes.shape({
          mediaItemUrl: PropTypes.string.isRequired,
          altText: PropTypes.string.isRequired,
          gatsbyImage: PropTypes.object.isRequired,
        }).isRequired,
      }).isRequired,
      content: PropTypes.string.isRequired,
      modified: PropTypes.string.isRequired,
      seo: PropTypes.shape({
        description: PropTypes.string.isRequired,
        focusKeywords: PropTypes.array,
        openGraph: PropTypes.shape({
          type: PropTypes.string.isRequired,
          slackEnhancedData: PropTypes.object,
        }).isRequired,
      }).isRequired,
      categories: PropTypes.shape({
        nodes: PropTypes.arrayOf(
          PropTypes.shape({
            slug: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            categoryBannerImages: PropTypes.object,
          }),
        ).isRequired,
      }).isRequired,
      postMetadata: PropTypes.shape({
        timeToRead: PropTypes.number,
        summary: PropTypes.string,
      }),
    }).isRequired,
    allFeaturedWpPost: PropTypes.shape({
      nodes: PropTypes.array.isRequired,
    }).isRequired,
    site: PropTypes.shape({
      siteMetadata: PropTypes.shape({
        siteUrl: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
  location: PropTypes.object.isRequired,
};

export default BlogSinglePost;
