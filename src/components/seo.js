import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useStaticQuery, graphql } from 'gatsby';
import { CommonContext } from '../context';

const SEO = ({ title, description, image, type, additionalData, noindex }) => {
  const { site } = useStaticQuery(query);
  const { location } = useContext(CommonContext);
  const {
    defaultTitle,
    defaultDescription,
    siteUrl,
    defaultImage,
    twitterUsername,
    titleTemplate,
  } = site.siteMetadata;

  const seo = {
    title: title || defaultTitle,
    description: description || defaultDescription,
    image: `${image || defaultImage}`,
    url: `${siteUrl}${location.pathname}`,
  };

  return (
    <Helmet title={seo.title} titleTemplate={titleTemplate}>
      <meta lang="en-GB" />
      <meta name="description" content={seo.description} />
      <meta name="image" content={seo.image} />

      {seo.url && <meta property="og:url" content={seo.url} />}

      {(type ? true : null) && <meta property="og:type" content={type} />}

      {seo.title && <meta property="og:title" content={seo.title} />}

      {seo.description && (
        <meta property="og:description" content={seo.description} />
      )}

      {seo.image && <meta property="og:image" content={seo.image} />}

      <meta name="twitter:card" content={seo.description} />

      {twitterUsername && (
        <meta name="twitter:creator" content={twitterUsername} />
      )}

      {additionalData &&
        additionalData.map((item, idx) => (
          <meta
            key={`twitter-meta-${idx}-label`}
            name={`twitter:label${idx + 1}`}
            content={item.label}
          />
        ))}

      {additionalData &&
        additionalData.map((item, idx) => (
          <meta
            key={`twitter-meta-${idx}-data`}
            name={`twitter:data${idx + 1}`}
            content={item.data}
          />
        ))}
      {seo.title && <meta name="twitter:title" content={seo.title} />}

      {seo.description && (
        <meta name="twitter:description" content={seo.description} />
      )}

      {seo.image && <meta name="twitter:image" content={seo.image} />}
      {noindex && <meta name="robots" content="noindex" />}
    </Helmet>
  );
};

export default SEO;

SEO.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
  type: PropTypes.string,
  additionalData: PropTypes.array,
  location: PropTypes.object,
  noindex: PropTypes.bool,
};

SEO.defaultProps = {
  title: null,
  description: null,
  additionalData: [],
  image: null,
  noindex: false,
};

const query = graphql`
  query SEO {
    site {
      siteMetadata {
        defaultTitle: title
        defaultDescription: description
        siteUrl: url
        defaultImage: image
        twitterUsername
        titleTemplate
      }
    }
  }
`;
