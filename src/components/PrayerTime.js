import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { checkLocation, getPrayerTime, prayerTimeSlugs } from '../utils';
import IconComponent from './IconComponent';
function PrayerTime() {
  const [prayerTimes, setPrayerTimes] = useState();
  const [address, setAddress] = useState();
  const [showPrayerTime, setShowPrayerTime] = useState(false);

  useEffect(() => {
    if (!localStorage.getItem('hidePrayerTimes')) {
      setShowPrayerTime(true);
    }
  }, []);

  useEffect(() => {
    if (showPrayerTime) {
      if (localStorage.getItem('location')) {
        const location = JSON.parse(localStorage.getItem('location'));
        setPrayerTimes(getPrayerTime(location));
        setAddress(location);
      } else {
        _checkLocation();
      }
    }
  }, [showPrayerTime]);

  const _setShowPrayerTime = (shouldShow) => {
    if (shouldShow) {
      localStorage.removeItem('hidePrayerTimes');
    } else {
      localStorage.setItem('hidePrayerTimes', shouldShow.toString());
    }

    window.location.reload();
  };

  const _checkLocation = async () => {
    try {
      const location = await checkLocation();
      localStorage.setItem('location', JSON.stringify(location));
      setAddress(location);
      setPrayerTimes(getPrayerTime(location));
    } catch (error) {
      setShowPrayerTime(false);
    }
  };

  return showPrayerTime ? (
    <div className="flex w-full text-xs text-center sm:text-sm bg-neutral-2">
      <div className="flex justify-between items-center md:justify-around h-8 p-2 sm:p-1.5 mx-auto space-x-1 text-center w-full max-w-7xl text-neutral-7">
        {prayerTimes && (
          <>
            {prayerTimeSlugs.map((item) => (
              <div
                key={item.slug}
                className={clsx(
                  item.slug == prayerTimes.currentPrayer() ? 'font-bold' : '',
                  item.slug == prayerTimes.nextPrayer()
                    ? 'md:bg-neutral-0 rounded-full font-bold md:animate-pulse'
                    : '',
                  item.slug == prayerTimes.currentPrayer() ||
                    item.slug == prayerTimes.nextPrayer()
                    ? 'xs:block'
                    : 'md:block hidden',
                  'px-2 cursor-pointer',
                )}
                onClick={() => {
                  window.location.href = '/prayer-time';
                }}
              >
                {item.label} :{' '}
                {prayerTimes[item.slug]
                  .toLocaleTimeString()
                  .replace(':00', ' ')}
              </div>
            ))}
          </>
        )}
        <button
          onClick={_checkLocation}
          title={
            address
              ? address.city +
                ', ' +
                address.country +
                ', ' +
                new Date().toLocaleDateString()
              : new Date().toLocaleDateString()
          }
          className="flex items-center justify-end flex-1 text-right sm:flex-initial"
        >
          {address && (
            <span className="hidden px-1 sm:block">
              {address.city}, {address.country_code}
            </span>
          )}
          <IconComponent name="gps-icon" />
        </button>
      </div>
      <button
        className="mr-5 justify-self-end text-neutral-0"
        title="Hide prayer times"
        onClick={() => {
          _setShowPrayerTime(false);
        }}
      >
        <IconComponent name="access-time-icon" className="text-neutral-7" />
      </button>
    </div>
  ) : (
    <button
      className="fixed left-0 p-2 rounded-br-full md:left-auto -top-2 md:top-0 md:rounded-b-full md:right-8 bg-neutral-2"
      onClick={() => {
        _setShowPrayerTime(true);
      }}
      title="Show Prayer Times"
    >
      <IconComponent name="pray-dua-icon" className="w-4 h-4 md:w-6 md:h-6" />
    </button>
  );
}

PrayerTime.propTypes = {
  location: PropTypes.object,
};

export default PrayerTime;
