import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import ClickAwayListener from 'react-click-away-listener';
import { ExternalLinkIcon } from '../custom-icons';

function Dropdown(props) {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const { component, menu, isResponsive, showExternalLinkIcon } = props;
  return (
    <ClickAwayListener onClickAway={() => setIsMenuOpen(false)}>
      <div>
        <button
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            setIsMenuOpen(!isMenuOpen);
          }}
        >
          {component}
        </button>
        {isMenuOpen ? (
          <div
            className={
              !isResponsive
                ? `absolute mt-2 w-72 rounded-3xl shadow-2xl bg-neutral z-10 py-4 right-0 md:right-auto text-accent`
                : `bg-neutral text-accent text-sm md:text-base md:absolute md:mt-2 md:w-72 md:rounded-3xl md:shadow-2xl md:z-10 md:py-4`
            }
          >
            <div className={isResponsive ? 'md:py-1' : 'py-1'}>
              {menu.map((item) =>
                !item.route.includes('http') ? (
                  <Link
                    key={item.route}
                    to={item.route}
                    className={`py-2 leading-6 tracking-wide font-regular ${
                      isResponsive ? 'px-4 md:px-8 md:h-14' : 'h-14 px-8'
                    } items-center flex hover:bg-neutral-1`}
                    onClick={(e) => {
                      e.stopPropagation();
                    }}
                  >
                    {item.title}
                  </Link>
                ) : (
                  <a
                    key={item.title}
                    href={item.route}
                    target="_blank"
                    rel="noreferrer"
                    className="flex items-center justify-between px-8 py-2 leading-6 tracking-wide font-regular h-14 hover:bg-neutral-1"
                    onClick={(e) => {
                      e.stopPropagation();
                    }}
                  >
                    {item.title}
                    {showExternalLinkIcon && (
                      <ExternalLinkIcon className="text-neutral-5" />
                    )}
                  </a>
                ),
              )}
            </div>
          </div>
        ) : null}
      </div>
    </ClickAwayListener>
  );
}

Dropdown.propTypes = {
  component: PropTypes.node,
  menu: PropTypes.array.isRequired,
  isResponsive: PropTypes.bool,
  showExternalLinkIcon: PropTypes.bool,
};

Dropdown.defaultProps = {
  isResponsive: false,
  showExternalLinkIcon: false,
};

export default Dropdown;
