import { createContext } from 'react';

export const CommonContext = createContext({
  location: undefined,
  setLocation: () => {},
});
