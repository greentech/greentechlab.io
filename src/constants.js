import GooglePlay from '../static/icons/google-play-store.svg';
import Apple from '../static/icons/apple-icon.svg';
import Windows from '../static/icons/windows.svg';
import WebBrowser from '../static/icons/web-browser.svg';
import Huawei from '../static/icons/huawei.svg';
import Linux from '../static/icons/linux-icon.svg';

export const appStoreNames = [
  {
    slug: 'playStore',
    label: 'Google Play Store',
    icon: GooglePlay,
    secondaryLabel: 'Play Store',
  },
  {
    slug: 'appStore',
    label: 'iOS App Store',
    icon: Apple,
    secondaryLabel: 'App Store',
  },
  {
    slug: 'huawei',
    label: 'Huawei App Gallery',
    icon: Huawei,
    secondaryLabel: 'App Gallery',
  },
  { slug: 'samsung', label: 'Samsung Store', icon: '' },
  {
    slug: 'mac',
    label: 'Mac App Store',
    icon: Apple,
    secondaryLabel: 'Mac',
  },
  { slug: 'opera', label: 'Opera Store', icon: 'opera' },
  {
    slug: 'windows',
    label: 'Windows Download',
    icon: Windows,
    secondaryLabel: 'Windows',
  },
  {
    slug: 'linux',
    label: 'Linux Download',
    icon: Linux,
    secondaryLabel: 'Linux',
  },
  {
    slug: 'web',
    label: 'Web App',
    secondaryLabel: 'Visit on',
    icon: WebBrowser,
  },
];

export const navBarLinks = [
  {
    title: 'Home',
    route: '/',
  },
  {
    title: 'Our Apps',
    route: '/apps',
  },
  {
    title: 'Blog',
    route: '/blog/',
  },
  {
    title: 'Organization',
    menu: [
      {
        title: 'About Us',
        route: '/about',
      },
      {
        title: 'Our Services',
        route: '/our-services',
      },
      {
        title: 'Contact',
        route: '/contact',
      },
      {
        title: 'Career & Volunteer',
        route: '/career-volunteer',
      },
      {
        title: 'Collaborate',
        route: '/collaborate',
      },
    ],
  },
  {
    title: 'Help & Support',
    menu: [
      {
        title: 'FAQs',
        route: '/support',
      },
      {
        title: 'Give Feedback',
        route: 'https://feedback.gtaf.org',
      },
    ],
  },
];

export const socialLinks = {
  facebook: 'https://www.facebook.com/GreenTech0',
  instagram: 'https://www.instagram.com/greentechapps',
  twitter: 'https://twitter.com/greentechapps',
  youtube: 'https://www.youtube.com/@greentechapps',
};

export const footerLinks = [
  {
    title: 'Career & Volunteer',
    route: '/career-volunteer',
  },
  { title: 'Privacy Policy', route: '/privacy-policy' },
  {
    title: 'Contact',
    route: '/contact',
  },
];

export const fadeUpVariants = {
  hidden: { opacity: 0, y: 40 },
  visible: (delay) => ({
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.6,
      delay: delay * 0.15,
      ease: [0.22, 1, 0.36, 1],
    },
  }),
};

export const blogConstants = {
  layout: {
    maxWidth: 'lg:max-w-[968px]',
    padding: 'p-6 pt-8 lg:pt-10 lg:p-0',
  },
  grid: {
    TocLayout: 'grid-cols-1 gap-8 lg:grid-cols-[288px_1fr]',
    NoTocLayout: 'grid-cols-1 gap-8',
  },
};

export const socialLinksWithIcon = [
  {
    icon: 'facebook-color-icon',
    url: 'https://www.facebook.com/sharer/sharer.php?u=',
  },
  {
    icon: 'x-color-icon',
    url: 'https://twitter.com/intent/tweet?text=@greentechapps&url=',
  },
  {
    icon: 'linkedin-color-icon',
    url: 'https://www.linkedin.com/shareArticle?url=',
  },
  {
    icon: 'whatsapp-color-icon',
    url: 'whatsapp://send?text=',
  },
  {
    icon: 'telegram-color-icon',
    url: 'https://telegram.me/share/url?url=',
  },
];